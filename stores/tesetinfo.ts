import {acceptHMRUpdate, defineStore} from "pinia";

interface TestRequest{
  subjectId : number
  teacherId : number
  abilityUnitFactorName : string
  abilityUnitName : string
  testType : string
  problemNum : number
  testTime : string
  testName : string
  score : number
  level : number
  dateTest : string
  ncs : string
}

interface TestDetail{
  subjectName : string
  abilityUnitName :string
  abilityUnitFactorName : string
  teacherName :string
  testName : string
  dateTest :string
  testTime : number
  testType : string
  isSetTest : string
  level : number
  registerHuman : number
  score : number
  problemNum : number
  ncs : number
}
export const useTestStore = defineStore('testInfo', {
  state: () => ({
    testInfoData:[] as TestRequest[],
    testDetail:[] ,
    subjectName: [],
    testName:[],
    currentPage: 1,
    pageSize: 10,
    totalItems: 0,
    totalPage: 0,
    totalCount:0,
  }),

  actions: {
    setTestInfo(data: TestRequest) {
      $fetch('http://35.193.177.229:8080/v1/subjectStatus/new',{
        method:'POST',
        body : data
      });
      console.log(data)
      console.log('hi')
    },
    async getTestInfoList() {
      try {
        const { data }: any = await useFetch(
          `http://35.193.177.229:8080/v1/subjectStatus/all`,
          {
            method: 'GET',
          }
        );
        if (data) {
          this.testInfoData = data.value.list;
          console.log(data.value.list);

        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    },

    async getTestInfoDetail(detailId:string){
      const {id} = useRoute().params
      const {data}: any = await useFetch(

        `http://35.193.177.229:8080/v1/subjectStatus/detail/subjectStatusId/${id}`,
        {
          method: 'GET',
        });
      if (data) {

      }
    },
    async getTestPaging(pageNum:number, pageSize:number){
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://35.193.177.229:8080/v1/subjectStatus/all/pageNum/${pageNum}`,
        {
          method: 'GET',
        });
      if (data) {
        this.testInfoData = data.value.list;
      }
    },

    async editTestInfo(subjectStatusId: string) {
      const { id } = useRoute().params;
      const token = useCookie('token');
      const { data }: any = await useFetch(
        `http://35.193.177.229:8080/v1/subjectStatus/changeInfo/subjectStatusId/${subjectStatusId}`,
        {
          method: 'GET',
        }
      );
      if (data) {
        this.testDetail = data.value.data;
        this.subjectName = data.value.data.subjectName;
      }
      return {
        testDetail: this.testDetail,
        testName: this.testName,
      };
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTestStore, import.meta.hot))
}
